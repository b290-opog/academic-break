// 1. JS - Function Parameters and Return Statement // JS - Selection Control Structures

function shipItem(item, weight, location){

    let totalShippingFee = 0;

    if(!item || !weight || !location || typeof(item) !== "string" || typeof(weight) !== "number" || typeof(location) !== "string"){
        return `Invalid inputs, check if information provided are correct and in exact order ("item" - weight - "location").`
    } else {
        if(weight <= 4){
            totalShippingFee += 150;
        } else if (weight >= 5 && weight <= 7 ){
            totalShippingFee += 280;
        } else if (weight >= 8 && weight <= 9 ){
            totalShippingFee += 340;
        } else if (weight == 10 ){
            totalShippingFee += 410;
        } else if (weight > 10){
            totalShippingFee += 560;
        };
    
        switch(location) {
            case "international":
                totalShippingFee += 250;
                return `The total shipping fee for an ${item} that will ship ${location} is ${totalShippingFee}.`
                break;
            case "local":
                return `The total shipping fee for an ${item} that will ship ${location} is ${totalShippingFee}.`
                break;
        };
    };
}

// 2 - 3. JS Object and Array Manipulation

class Product {
    constructor(name, price, quantity) {
        this.name = name;
        this.price = Number(price);
        this.quantity = Number(quantity);
    }
}

console.warn(`Results of instantiating new Product:`);
let dogFood = new Product("Dog Food", 129, 10);
console.log(dogFood);


class Customer {
    constructor(name) {
        this.name = name;
        this.cart = [];
        this.addToCart = product => {
            this.cart.push(product);
            return `${product} is added to cart.`;
        };
        this.removeToCart = product => {
            this.cart.splice((this.cart.indexOf(product)), 1);
            return `${product} is removed from the cart`;
        };
    }
}

console.warn(`Results of instantiating new Customer:`);
let mithOpog = new Customer("Mith Opog");
console.log(mithOpog);

console.warn(`Results of adding items to Customer.cart:`);
console.log(mithOpog);
console.log(mithOpog.addToCart("Dog Food"));
console.log(mithOpog.addToCart("Cat Food"));
console.log(mithOpog);

console.warn(`Results of removing items to Customer.cart:`);
console.log(mithOpog.removeToCart("Dog Food"));
console.log(mithOpog);



